FROM php:apache
########
# Customize Apache and php config to your needs
########
# # Enable .htaccess support
# RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/apache2/apache2.conf && \
# # enable rewrite engine
# a2enmod rewrite && \
# # use php development config
# mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" && \
# # install additional system packages
# apt update && apt install -y --no-install-recommends \
# # library required by imagick
# libmagickwand-dev \
# # library required by intl
# libicu-dev && \
# # Install and enable php packages
# pecl install imagick && \
# docker-php-ext-install intl gettext mysqli && \
# docker-php-ext-enable imagick