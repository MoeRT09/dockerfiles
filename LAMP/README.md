# LAMP (Linux Apache MariaDB PHP) Development Environment for Docker
This directory contains a Docker Compose configuration for a LAMP-Stack. For easier database management, it also provides an instance of [phpMyAdmin](https://www.phpmyadmin.net/).

## Usage
- Starting your development environment is as simple as running `docker-compose up -d` from your terminal, while having your current working directory point to this directory.
- To stop the environment execute `docker-compose stop`.
- Every PHP/HTML file stored under the `web` directory will be served on http://localhost:8080.
- In order to access phpMyAdmin, visit http://localhost:8085.
- The default password for the MariaDB root user is `docker`.
- See [apache.dockerfile](apache.dockerfile) for examples on how to modify configuration files and adding PHP extensions. **If you don't need the extensions listed there, comment them out, otherwise they will bloat the container and slowdown the build process!**
- **Note:** Modifying the Dockerfile requires a rebuild of the web container. This can be accomplished by issuing `docker-compose build`.

## Database import / export
- For quickly **exporting** the current state of the database (without using phpMyAdmin) run `docker-compose exec db mysqldump --add-drop-table -u root -pdocker wordpress > wordpress.sql`.
- An **import** can be accomplished by running `docker-compose exec -T db mysql -u root -pdocker wordpress < wordpress.sql`.  
**Warning: this will overwrite your existing database!**
