# Dockerfiles for Common Development Tasks
This repository contains Docker- and Docker-Compose files for common development tasks.


**Currently available configurations:**
- [LAMP](LAMP/) Provides a Linux, Apache, MariaDB, PHP-Stack
- [WordPress](WordPress/) Provides a special LAMP-Stack intended for WordPress development

## Prerequisites
You need to have docker as well as docker-compose installed.
### Linux
See the [docker documentation](https://docs.docker.com/install/linux/docker-ce/ubuntu/) for installation instructions for your distro. Install **docker-compose** like mentioned [here](https://docs.docker.com/compose/install/).
### Windows and Mac
Install [Docker Desktop](https://www.docker.com/products/docker-desktop). This should already include **docker-compose**.